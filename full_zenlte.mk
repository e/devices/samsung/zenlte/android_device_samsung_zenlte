PRODUCT_RUNTIMES := runtime_libart_default

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

$(call inherit-product, device/samsung/zenlte/device.mk)

PRODUCT_NAME := full_zenlte
PRODUCT_DEVICE := zenlte
PRODUCT_BRAND := Samsung
PRODUCT_MODEL := SM-928F
PRODUCT_MANUFACTURER := samsung
