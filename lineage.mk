# Inherit from the common Open Source product configuration

#
# This file is the build configuration for a full Android
# build for zerofltexx hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and zerofltexx, hence its name.
#

# needed ?
#PRODUCT_RUNTIMES := runtime_libart_default


#$(call inherit-product, vendor/moto/shamu/device-vendor.mk)
$(call inherit-product, device/samsung/zenlte/full_zenlte.mk)

TARGET_SCREEN_HEIGHT := 2560
TARGET_SCREEN_WIDTH := 1440

$(call inherit-product, vendor/cm/config/common_full_phone.mk)

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME="zenlte" \
		TARGET_DEVICE="zenlte"


PRODUCT_NAME := lineage_zenlte
PRODUCT_DEVICE := zenlte

#BUILD_FINGERPRINT := samsung/zerofltexx/zeroflte:7.0/NRD90M/G920FXXU5EQG3:user/release-keys

## add other values from https://source.android.com/setup/develop/new-device#prod-def
