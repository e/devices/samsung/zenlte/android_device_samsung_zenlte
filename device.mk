# Create a device.mk makefile that declares the files and modules needed for the device. For an example, see device/moto/shamu/device.mk.es

LOCAL_PATH := device/samsung/zenlte

## device overlays
DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay

# Audio
#PRODUCT_COPY_FILES += \
#    $(LOCAL_PATH)/configs/audio/mixer_paths_0.xml:system/etc/mixer_paths_0.xml

## (2) Also get non-open-source specific aspects if available
$(call inherit-product-if-exists, vendor/samsung/zenlte/zenlte-vendor.mk)

# Carrier init
#PRODUCT_PACKAGES += \
#    init.carrier.rc

# cpboot daemon
#PRODUCT_COPY_FILES += \
#    $(LOCAL_PATH)/ril/sbin/cbd:root/sbin/cbd

# Inherit from zero-common
$(call inherit-product, device/samsung/zero-common/zero-common.mk)
