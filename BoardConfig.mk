# inherit from zero-common
include device/samsung/zero-common/BoardConfigCommon.mk

# Assert
TARGET_OTA_ASSERT_DEVICE := zenltexx,zenlte

# Include path
TARGET_SPECIFIC_HEADER_PATH += device/samsung/zenlte/include

# Kernel
TARGET_KERNEL_CONFIG := lineageos_zenlte_defconfig

# Partitions
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 3879731200

# Radio
BOARD_MODEM_TYPE := ss333
